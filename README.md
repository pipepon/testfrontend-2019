# TestFrontEnd-2019

It is a project to a test, this is to evaluate my knowledge about frontend

Pasos para la ejecucion del proyecto.


1. Es necesario usar una version de node de 8 o inferior para el
   correcto funcionamiento del grunt.

2. Dentro de la carpeta del projecto ejecutar un npm install para 
   instalar las dependencias del proyecto. Esto con el fin de 
   que funcione el grunt y el less.

3. Ejecutar un grunt para generar el archivo css a partir del less.
