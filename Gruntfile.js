module.exports = function (grunt) {
    
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            less: {
                files: ['project/less/*'],
                tasks: ['less'],
            }
        },
        less: {
            custom: {
                expand: true,
				cwd: 'project/less/',
                src: '**/*.less',
                dest: 'project/generated/style',
                ext: '.css'
            },
        },
	
    });
    // Plugins
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.registerTask('default', ['less']);
};