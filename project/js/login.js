
function showAlert () {
    
    $( ".login-form" ).submit(function( event ) {

        event.preventDefault();

        if($('.login-form-textbox.email').val().length > 0 && $('.login-form-textbox.password').val().length > 0){
            
            $(".js-event-alert.success").toggleClass('d-none');

            setTimeout(function() {
                $(".js-event-alert.success").toggleClass('d-none');
            }, 2000);
        }
        else{

            $(".js-event-alert.danger").toggleClass('d-none');
            
            setTimeout(function() {
                $(".js-event-alert.danger").toggleClass('d-none');
            }, 2000);
        }
    });
}

$(document).ready(function () {
	showAlert();
});